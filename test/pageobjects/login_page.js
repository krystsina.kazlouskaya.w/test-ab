class LoginPage {
    get LoginInput() {
        return '[id="loginform-username"]';
    }

    get PasswordInput() {
        return '[id="loginform-password"]'
    }

    get LoginNewButton() {
        return '[name="login-button"]'
    }
}

module.exports = new LoginPage();