class mainPage {
    get goodsInBasket() {
        return '[class="basket-count-items badge badge-primary"]';
    }

    get goodWithoutDiscount() {
        return '(//*[@class="note-item card h-100"]/div[2]/button)[1]';
    }

    get goodWithDiscount() {
        return '(//*[@class="note-item card h-100 hasDiscount"]/div[2]/button)[1]';
    }

    get basketBall() {
        return '[id="basketContainer"]';
    }

    get clearBasket() {
        return '[class="btn btn-danger btn-sm mr-auto"]';
    }

    get modalBasket() {
        return '[class="dropdown-menu dropdown-menu-right show"]'
    }

    get goodsName() {
        return '[class="basket-item-title"]'
    }

    get price() {
        return '[class="basket-item-price"]'
    }

    get totalPrice() {
        return '[class="basket_price"]'
    }

    get toBasketButton() {
        return '[class="btn btn-primary btn-sm ml-auto"]'
    }

    get oneBook() {
        return '(//*[@class="actionBuyProduct btn btn-primary mt-3"])'
    }

    get secondPage() {
        return '(//*[@class="page-link"])[2]'
    }

}

module.exports = new mainPage();