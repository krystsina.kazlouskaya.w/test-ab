const steps = require('../../steps/steps');

describe('Тест-кейс 4. Переход в корзину с 9 разными товарами and here I test Fetch 1234 changes', () => {
    it('Тест 4', async () => {
        await steps.openPage();
        await steps.setValueToLogin();
        await steps.setValueToPassword();
        await steps.clickButtonLogin();
        await steps.reloadPage();
        await steps.clearBasket();
        await steps.clickBuyWithDiscount();
        await steps.notEmptyBasket();
        await steps.addEightGoods();
        await steps.clickBasketButton();
        await steps.modalBasketIsDisplayed();
        await steps.goodsNameIsDisplayed();
        await steps.priceIsDisplayed();
        await steps.totalPriceIsDisplayed();
        await steps.clickToBasket();
        await steps.checkBasketPage();
    })

})