const steps = require('../../steps/steps');

describe('Тест-кейс 5. Переход в корзину с 9 акционными товарами одного наименования case 5 changes', () => {
    it('Тест 5', async () => {
        await steps.openPage();
        await steps.setValueToLogin();
        await steps.setValueToPassword();
        await steps.clickButtonLogin();
        await steps.reloadPage();
        await steps.clearBasket();
        await steps.clickBuyWithDiscount();
        await steps.notEmptyBasket();
        await steps.addGoods();
        await steps.clickBasketButton();
        await steps.modalBasketIsDisplayed();
        await steps.goodsNameIsDisplayed();
        await steps.priceIsDisplayed();
        await steps.totalPriceIsDisplayed();
        await steps.clickToBasket();
        await steps.checkBasketPage();
    })

})

//После шага clickBasketButton тест падает, так как в корзину добавлено 9 товаров (когда в корзину добавлено 9 товаров,)
//модальное окно корзины не открывается, а сразу происходит переход на страницу корзины