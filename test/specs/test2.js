const steps = require('../../steps/steps');

describe('Тест-кейс 2. Переход в корзину с 1 неакционным товаром case 2 changes', () => {
    it('Тест 2', async () => {
        await steps.openPage();
        await steps.setValueToLogin();
        await steps.setValueToPassword();
        await steps.clickButtonLogin();
        await steps.clearBasket();
        await steps.clickBuyButton();
        await steps.notEmptyBasket()
        await steps.clickBasketButton();
        await steps.modalBasketIsDisplayed();
        await steps.goodsNameIsDisplayed();
        await steps.priceIsDisplayed();
        await steps.totalPriceIsDisplayed();
        await steps.clickToBasket();
        await steps.checkBasketPage();
    })

})