const steps = require('../../steps/steps');

describe('Тест-кейс 3. Переход в корзину с 1 акционным товаром новое изменение test fetch case 3 changes', () => {
    it('Тест 3', async () => {
        await steps.openPage();
        await steps.setValueToLogin();
        await steps.setValueToPassword();
        await steps.clickButtonLogin();
        await steps.clearBasket();
        await steps.clickBuyWithDiscount();
        await steps.notEmptyBasket();
        await steps.clickBasketButton();
        await steps.modalBasketIsDisplayed();
        await steps.goodsNameIsDisplayed();
        await steps.priceIsDisplayed();
        await steps.totalPriceIsDisplayed();
        await steps.clickToBasket();
        await steps.checkBasketPage();
    })
})