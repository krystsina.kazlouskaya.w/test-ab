const steps = require('../../steps/steps');
const start = require('@wdio/allure-reporter').default.startStep;
const end = require('@wdio/allure-reporter').default.endStep;

describe(`Тест-кейс 1. Переход в пустую корзину changes`, () => {
    it('Тест 1', async () => {
        await steps.openPageNew();
        await steps.setValueToLoginNew();
        await steps.setValueToPasswordNew();
        await steps.clickButtonLogin();
        await steps.clearBasket();
        await steps.clickBasketButton();
        await steps.modalBasketIsDisplayed();
        await steps.goodsNameIsDisplayed();
        await steps.priceIsDisplayed();
        await steps.totalPriceIsDisplayed();
        await steps.clickToBasket();
        await steps.checkBasketPage();
    })

})

// Тест падает на шаге clickBasketButton - модальное окно корзины не открывается