const {assert} = require('chai');
const start = require('@wdio/allure-reporter').default.startStep;
const end = require('@wdio/allure-reporter').default.endStep;
const allure = require('@wdio/allure-reporter').default;
const loginPage = require('../test/pageobjects/login_page');
const elementManager = require('../helper/elementManager')
const dataTest = require('../helper/valueData');
const mainPage = require('../test/pageobjects/mainPage')
const axios = require('axios');

class Steps {
    async openNewPage() {
        await start(`Открытие страницы: ${process.env.URL}`);
        await browser.url(process.env.URL);
        await browser.pause(1000);
        await end();
    }


    async setValueToLogin() {
        await start(`Ввод Логина`);
        await elementManager.setValue(
            loginPage.LoginInput,
            dataTest.login
        );
        await browser.pause(1000)
        await end();
    }

    async setValueToPassword() {
        await start(`Ввод Пароля`);
        await elementManager.setValue(
            loginPage.PasswordInput,
            dataTest.password
        );
        await browser.pause(1000)
        await end();
    }

    async clickButtonLogin() {
        await start(`Нажатие Кнопки "Войти"`);
        await elementManager.click(loginPage.LoginButton);
        await browser.pause(1000)
        await end();
    }


    async notEmptyBasket() {
        await start(`Проверка Не Пустой Корзины`);
        await elementManager.scroll(mainPage.goodsInBasket);
        const value = await elementManager.getNumber(mainPage.goodsInBasket);
        await assert.equal(
            value,
            1,
            `Корзина пустая`
        );
        await browser.pause(1000)
        await end();
    }

    async clickBuyButton() {
        await start(`Нажатие Кнопки "Купить"`);
        await elementManager.click(mainPage.goodWithoutDiscount);
        await browser.pause(1000)
        await end();
    }

    async clickBuyWithDiscount() {
        await start(`Нажатие Кнопки "Купить" На Товар Со Скидкой`);
        await elementManager.click(mainPage.goodWithDiscount);
        await browser.pause(1000)
        await end();
    }

    async clickBasketButton() {
        await start(`Открыть Модальное Окно Корзины`);
        await elementManager.click(mainPage.basket);
        await browser.pause(1000)
        await end();
    }

    async clickClearBasket() {
        await start(`Нажать "Очистить Корзину"`);
        await elementManager.click(mainPage.clearBasket);
        await browser.pause(1000)
        await end();
    }

    async modalBasketIsDisplayed() {
        await start(`Проверка Отображение Модального Окна Корзины`);
        await elementManager.isDisplayed(mainPage.modalBasket)
        await browser.pause(1000)
        await end();
    }

    async goodsNameIsDisplayed() {
        await start(`Проверка Отображение Товаров В Корзине`);
        await elementManager.isDisplayed(mainPage.goodsName)
        await browser.pause(1000)
        await end();
    }

    async priceIsDisplayed() {
        await start(`Проверка Отображения Цен В Корзине`);
        await elementManager.isDisplayed(mainPage.price)
        await browser.pause(1000)
        await end();
    }

    async totalPriceIsDisplayed() {
        await start(`Проверка Отображения Полной Стоимости В Корзине`);
        await elementManager.isDisplayed(mainPage.totalPrice)
        await browser.pause(2000)
        await end();
    }

    async clickToBasket() {
        await start(`Нажатие На Кнопку "Перейти В Корзину"`);
        await elementManager.click(mainPage.toBasketButton)
        await browser.pause(2000)
        await end();
    }

    async checkBasketPage() {
        await start(`Открытие страницы: ${dataTest.basket}`);
        await browser.pause(2000)
        const value = await browser.getUrl();
        console.log(value);
        await assert.equal(
            value,
            'https://enotes.pointschool.ru/basket',
        );
        await end();
    }

    async addEightGoods() {
        await start(`Добавление 8 Товаров В Корзину`);
        for (let i = 2; i < 9; ++i) {
            const book = `${mainPage.oneBook}[${i}]`
            await browser.pause(2000);
            await elementManager.click(book);
            await browser.pause(1000);
            const value = await elementManager.getNumber(mainPage.goodsInBasket);
            if (value == '8') {
                await elementManager.scroll(mainPage.secondPage);
                await elementManager.click(mainPage.secondPage)
                await browser.pause(1000);
                await elementManager.click(`${mainPage.oneBook}[1]`)
            }

        }
        await end();
    }


    async addGoods() {
        await start(`Добавление 9 Товаров Одного Наименования В Корзину`);
        let i = 0;
        do {
            await elementManager.scroll(mainPage.goodWithDiscount);
            await elementManager.click(mainPage.goodWithDiscount)
            await browser.pause(1000)
            i++;
        } while (i < 10);
        await end();
    }

    async reloadPage() {
        await browser.pause(2000)
        await start(`Перезагрузка Страницы`);
        // await start('Перезагрузка страницы');
        await browser.refresh();
        // await end();
        await end();
    }

    async clearBasket() {
        await start(`Очистка Корзины`);
        await browser.pause(2000);
        const value = await elementManager.getNumber(mainPage.goodsInBasket);
        if (value !== '0') {
            await this.clickBasketButton();
            await this.clickClearBasket()
        }
        await end();
    }

}

module.exports = new Steps();
