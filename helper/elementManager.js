module.exports = {


    async setValue(locator, value) {
        const selector = await $(locator);
        await selector.setValue(value);
    },

    async click(locator) {
        const selector = await $(locator);
        await selector.click();
    },

    async getNumber(locator) {
        const selector = await $(locator);
        return selector.getText();
    },

    async isDisplayed(locator) {
        const selector = await $(locator);
        return selector.isDisplayed();
    },

    async scrolling(locator) {
        const selector = await $(locator);
        await selector.scrollIntoView({block: 'center'});
    },


};
